<?php


namespace Dracoder\Extensions;


use DateInterval;
use Dracoder\Exceptions\Handlers\ExceptionHandler;
use Exception;


class DateIntervalExtension extends DateInterval
{
    /**
     * Nota: Utilizamos este método cuando requerimos una fecha para controlar
     *     la excepción que podría dar el constructor de \DateInterval.
     *
     * @param string $dateInterval
     */
    public function __construct($dateInterval)
    {
        try {
            parent::__construct($dateInterval);
        } catch (Exception $e) {
            (new ExceptionHandler($e))->handle(false);
        }
    }
}