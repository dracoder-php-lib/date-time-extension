<?php


namespace Dracoder\Extensions\DateUtils;


use DateTime;

class DateBasedPathGenerator
{
    private $relativePath = '';
    private $absolutePath = '';

    /**
     * GeneradorRutaGestion constructor.
     * Create the folders if it's necessary and fill the return vars.
     *
     * @param string $projectDir
     * @param string $baseDir
     * @param DateTime $date
     * @param string $filename
     */
    public function __construct($projectDir, $baseDir, $date, $filename)
    {
        $year = $date->format('Y');
        $month = $date->format('n');

        $absolutePath = $projectDir . $baseDir;

        if (!is_dir($absolutePath)) {
            mkdir($absolutePath);
        }

        if (!is_dir($absolutePath . $year)) {
            mkdir($absolutePath . $year);
        }

        if (!is_dir($absolutePath . $year . '/' . $month)) {
            mkdir($absolutePath . $year . '/' . $month);
        }

        $this->absolutePath = $absolutePath . $year . '/' . $month . '/' . $filename . '.pdf';
        $this->relativePath = $baseDir . $year . '/' . $month . '/' . $filename . '.pdf';
    }

    /**
     * Get the absolute path
     *
     * @return string
     */
    public function getAbsolutePath()
    {
        return $this->absolutePath;
    }

    /**
     * Get the relative path
     *
     * @return string
     */
    public function getRelativePath()
    {
        return $this->relativePath;
    }
}